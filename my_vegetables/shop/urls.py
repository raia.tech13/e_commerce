from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('admin_panel', views.admin_panel, name="admin_panel"),
    path('user_panel', views.user_panel, name="user_panel"),
]
from django import forms
from .models import Product, Items, Cart


class AdminForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'price', 'tva', 'stock_max', 'stock_orderer']

class UserForm(forms.ModelForm):
    cart_id = forms.ModelChoiceField(queryset=Cart.objects.all(), widget=forms.HiddenInput())
    product_id = forms.ModelChoiceField(queryset=Product.objects.all(), widget=forms.HiddenInput())
    quantity = forms.IntegerField(initial=1, min_value=1)
    class Meta:
        model = Items
        fields = ['cart_id', 'product_id', 'quantity']
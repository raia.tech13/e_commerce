from django.db import models




class Product(models.Model):

    class Tva(models.TextChoices):
        tva_min = '5,50'
        tva_max = '20'

    name = models.CharField(max_length=40)
    price = models.FloatField(default=0)
    tva = models.CharField(max_length=4, choices=Tva.choices)
    stock_max = models.PositiveIntegerField(default=0)
    stock_orderer = models.PositiveIntegerField(default=0)
    
    def __str__(self):
        return self.name

    @property
    def TTC(self):
        price_ttc = price*(1+tva/100)
        return self.price_ttc

    


class Cart(models.Model):
    check_out = models.BooleanField(default=False)

    def __str__(self):
        return str(self.check_out)

class Items(models.Model):

    cart_id = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name="basket_number")
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="product_name")
    quantity = models.PositiveIntegerField(default=0)
    
    def __str__(self):
        return self.name


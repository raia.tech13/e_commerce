from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from .forms import AdminForm, UserForm
from .models import Product, Items, Cart


def index(request):
    return render(request, 'shop/index.html')


def admin_panel(request):
    form = AdminForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
        	form.save()
        	form = AdminForm()
        else:
            return redirect('index')
    context = {
        'form': form,
        }   
    return render(request, 'shop/admin.html', context)

def user_panel(request):
	shop_list = Product.objects.all()
	#new_cart = Cart.objects.create()

	form = UserForm(request.POST or None)
	if request.method == 'POST':
		if form.is_valid():
			form.save()
			form = UserForm()
		else:
			return redirect('user_panel')
	context = {
	'form': form,
	'shop_list': shop_list,
	}
	return render(request, 'shop/user.html', context)